def get_sdp(d):
    # build symbolic matrix D with D_{ij} = c_i * c_j
    D_temp = [[var('d_%d_%d' % (i,j)) for j in srange(i+1)] for i in srange(ceil(d/2)+1)]
    D =  matrix(SR, ceil(d/2)+1)
    for i in srange(ceil(d/2)+1):
        for j in srange(i):
            D[i,j] = D_temp[i][j]
        for j in srange(i,ceil(d/2)+1):
            D[i,j] = D_temp[j][i]
    # c_0 = -c_1
    D[0,1:] = -D[1,1:]
    D[1:,0] = -D[1:,1]
    D[0,0] = D[1,1]
    
    # build equations
    coeffs = []
    for k in srange(d+1):
        coeff = 0
        for i in srange(ceil(k/2), min(ceil(d/2),k)+1):
            coeff += D[i,i] * binomial(i,k-i) * binomial(k,i)
        for j in srange(ceil((k+1)/2), min(ceil(d/2),k)+1):
            for i in srange(k-j, j):
                coeff += 2*D[i,j]*binomial(i,k-j)*binomial(k,i)
        coeffs.append(coeff)
    coeffs[0] += 1
    coeffs[1] -= 1
    eqs = [coeffs[i] - coeffs[i%2] == 0 for i in srange(len(coeffs))]
    
    variables = []
    for i in srange(1,len(D_temp)):
        for j in srange(1,len(D_temp[i])):
            variables.append(D_temp[i][j])
      
    # solve equation system
    sol = solve(eqs[2:],variables)
    #TODO: check whether there is a solution    
    sol_lhs = list([eq.lhs() for eq in sol[0]])
    sol_rhs = list([eq.rhs() for eq in sol[0]])
    M = D.subs(dict(zip(sol_lhs,sol_rhs)))
    M = M[1:,1:]
    r_variables = list(M.variables())
    
    # extract matrices A_i and M
    A = [matrix(SR,M.nrows()) for _ in srange(len(r_variables))]
    for k in srange(len(r_variables)):
        for i in srange(M.nrows()):
            for j in srange(i+1):
                A[k][i,j] = M[i,j].coefficient(r_variables[k])
                A[k][j,i] = A[k][i,j] 
                M[i,j] -= A[k][i,j]*r_variables[k]
                M[j,i] = M[i,j]
    
    return (A,M)

def get_bounds(A,M):
    A_real = [matrix(RR,Ai) for Ai in A]
    M_real = matrix(RR,M)
    dim = M.nrows()
    p = SemidefiniteProgram()
    x = p.new_variable()
    p.add_constraint(sum([A_real[i]*x[i] for i in srange(len(A))]) + M_real >= matrix.zero(dim,dim,sparse=True))
    p.set_objective(x[0])
    opt = p.solve(objective_only=True)
    a_max = opt
    p.set_objective(-x[0])
    opt = - p.solve(objective_only=True)
    a_min = opt
    return (a_min,a_max)

def find_nice_fraction_in_interval(a,b):
    length = b - a
    if length >= 1: # there is for sure an integer in [a,b]
        return round((a + b)/2)
    else:
        error = length/2 - 10^(-7)*length
        return (RR((a + b)/2)).nearby_rational(max_error=error) 

def find_psd_matrix(A,M):
    while len(A) > 0:
        (a_min,a_max) = get_bounds(A,M)
        a = find_nice_fraction_in_interval(a_min,a_max)
        M += a*A[0]
        A = A[1:]
    return M

def cholesky_naive(D):
    n = D.nrows()
    C = matrix(SR,n)
    C[0,0] = sqrt(D[0,0])
    for i in srange(0,n):
        C[i,0] = D[i,0]/C[0,0]
    for j in srange(1,n):
        C[j,j] = sqrt(D[j,j] - sum([C[j,k]^2 for k in srange(j)]))
        for i in srange(j+1,n):
            C[i,j] = (D[i,j] - sum([C[j,k]*C[i,k] for k in srange(j)]))/C[j,j]
    return C

def find_certificate(d):
    (A,M) = get_sdp(d)
    D = find_psd_matrix(A,M)
    C = cholesky_naive(D)
    return C
